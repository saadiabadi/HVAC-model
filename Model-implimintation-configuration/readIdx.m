function  [trainIdx, valIdx,testIdx]=readIdx(trainIdxPath,valIdxPath,testIdxPath)
    trainIdx=csvread(trainIdxPath);
    valIdx=csvread(valIdxPath);
    testIdx=csvread(testIdxPath);
end

