function [ net tr ] = obtainFitNet(neur,epochs,lr,view, trainIdx,valIdx,testIdx,inputs,outputs)

           
          
            %%%
            %%Start network configuration
            %%%
            net = fitnet(neur);
            net.trainFcn='traingd';
            net.inputs{1}.processFcns = {'removeconstantrows','mapminmax'};
            net.outputs{2}.processFcns = {'removeconstantrows','mapminmax'};
            net.trainParam.lr=lr; 
            net.performFcn = 'mse'; 
            net.trainParam.epochs=epochs; 
            net.trainParam.showWindow = view; %Training visualization
            net.divideFcn = 'divideind';
            net.divideParam.trainInd=trainIdx;
            net.divideParam.valInd=valIdx;
            net.divideParam.testInd=testIdx;
            net.divideMode='sample';
            net.trainParam.max_fail =epochs;%Early termination condition.
            %%%
            %%End network configuration
            %%%
            
            %%%train
             [net,tr] = train(net,inputs',outputs');
end

