%This script is focised on checking regressors not related to the time
%Inputs, outputs and index are CSV that use the comma as separator


t=6;
inputsPath='patterns/105/inputs_105_2016-2017.csv';
%%%%T6%%%
disp(sprintf('T%i',t));
outputsPath=sprintf('patterns/105/outputs_105_2016-2017-T%i.csv',t);
%%%%T12%%%%
% disp('T12');
% outputsPath='patterns/105/outputs_105_2016-2017-T12.csv';
%%%%%%%%%%%
%%%%T18%%%%
 %disp('T18');
% outputsPath='patterns/105/outputs_105_2016-2017-T18.csv';
%%%%%%%%%%%

 outputs=csvread(outputsPath);
 
 trainIdx=csvread('index/idxTrain2016.csv');
 valIdx=csvread('index/idxVal2016.csv');
 
 testIdx=csvread('index/idxTest2017.csv');
 
 staticP=zeros(10,6);
 staticR=zeros(10,6);
 
 lambdaP=zeros(10,6);
 lambdaR=zeros(10,6);
 
 lambdaAP=zeros(10,6);
 lambdaAR=zeros(10,6);
 
 adaptP=zeros(10,6);
 adaptR=zeros(10,6);
 
  
disp('Starting test...')
if(size(trainIdx,1)==size(valIdx,1) && size(trainIdx,1)==size(testIdx,1))
   for idx=2:2 %size(trainIdx,1)
    ridx=1;
  
 %%Loading best net 2016%%%
 netPath=sprintf('nets/T%i/best_net2016T%i_%i.m',t,t,idx); 
%  netPath='/home/david/Data/datosCiTIUS/sistemaTemperatura/despacho105/data4NN/best_net2016T12.m'; 
%  netPath='/home/david/Data/datosCiTIUS/sistemaTemperatura/despacho105/data4NN/best_net2016T18.m'; 
 load(netPath,'-mat');
 net=best_net.net;
 tr=best_net.tr; 
 %%%End Loading Net%%%
 mlp= outputs(testIdx(idx,:));
 lambda= outputs(testIdx(idx,:)); 
 lambdaAdapt= outputs(testIdx(idx,:)); 
 adapt= outputs(testIdx(idx,:)); 
       
       for ruido=[0 0.05 0.1 0.15 0.20 0.25]
            inputs=csvread(inputsPath);
           disp('Ruido:');disp(ruido);
                %%%Start Creating noise%%%
                if ruido>0
                    cols=[2,4,5,6,8];
                    seed=idx;
                    inputs=createNoise(inputs,cols,testIdx(idx,:),ruido,seed);
                end
                %%% End Creating noise%%%
csvwrite(sprintf('results/T%i/pred_results/patterns-noise/patterns_noise_%g.csv',t,ruido),inputs(testIdx(idx,:),:));


            %%%Start Test FitNet with noise%%%
%                 [trainP, trainR ,valP ,valR ,testP, testR,testpred ] =getPerformanceNet ( net, tr, inputs,outputs);
%                 mlp=[mlp testpred];
%                 staticP(idx,ridx)=testP;
%                 staticR(idx,ridx)=testR;
%                 disp('***** static ******');
%                 disp(testP);
%                
%                 disp(testR);
%                  
%             %%%End Test FitNet%%%
% 
% 
%             %%%Start FitNet%%%
% %                 [ bestNet bestTr bestTrainP bestTrainR bestValP bestValR bestTestP bestTestR ]=checkFitNet(inputs, outputs, trainIdx(idx,:),valIdx(idx,:),testIdx(idx,:),idx);                 
% %                 avgTestPerformance(idx)=bestTestP;
% %                 avgTestR(idx)=bestTestR;
% %                 best_net.net=bestNet;
% %                 best_net.tr=bestTr;
% %                 save(sprintf('nets/T%i/best_net2016T%i_%i.m',t,t,idx),'best_net');
%              %%%End FitNet%%%
% 
%             %%%Start Lambda Net%%%
% %              tic
%              [tR,Tp,predOutputs]=checkLambdaNet( net, inputs, outputs, trainIdx(idx,:),valIdx(idx,:),testIdx(idx,:),idx);
%              
%              lambda= [lambda predOutputs']; 
%              lambdaP(idx,ridx)=Tp;
%              lambdaR(idx,ridx)=tR;
%             disp('***** lambda ******');
%                 disp(Tp);
%                 
%                 disp(tR);
%                 
% %              toc
% %             %%%End LambdaNet%%%
% % 
% % 
% %             %%%%Start Adapt-Lambda Net%%%%
% %                 tic
%                 [tR_al,Tp_al, predOutputs1]=checkAdaptLambdaNet( net, inputs, outputs, trainIdx(idx,:),valIdx(idx,:),testIdx(idx,:),idx);
%                lambdaAdapt= [lambdaAdapt predOutputs1']; 
%                 lambdaAP(idx,ridx)=Tp_al;
%                 lambdaAR(idx,ridx)=tR_al;
%                  disp('***** lambda adapt ******');
%                 disp(Tp_al);
%                 
%                 disp(tR_al);
%               
%                
%                 
% %                 toc
% %             %%%%End Adapt Net%%%%
% %                
% %             %%%%Start Adapt Net 
% %                 tic
%                 [tRa,Tpa, predOutputs2]=checkAdaptNet( net, inputs, outputs, trainIdx(idx,:),valIdx(idx,:),testIdx(idx,:),idx);
%                 adapt= [adapt predOutputs2'];
%                 adaptP(idx,ridx)=Tpa;
%                 adaptR(idx,ridx)=tRa;
%                 disp('***** Adaptive ******');
%                 disp(Tpa);
%                 
% %                 disp(tRa);
% %              
% %                 toc
%                 
%           ridx=ridx+1;      
%            disp(ridx);
          
       end
       sms=sprintf('Index %i \n',idx);
 disp(sms);
           
   end
%   csvwrite(sprintf('results/T%i/pred_results/static.csv',t),mlp);
%    csvwrite(sprintf('results/T%i/pred_results/lambda.csv',t),lambda);
%     csvwrite(sprintf('results/T%i/pred_results/lambdaAdapt.csv',t),lambdaAdapt);
%      csvwrite(sprintf('results/T%i/pred_results/adapt.csv',t),adapt);
%    
   
%  checkGRNN(inputs, outputs, trainIdx,valIdx,testIdx);
end

