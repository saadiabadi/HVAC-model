function [trainP, trainR ,valP ,valR ,testP, testR,testpred] =getPerformanceNet ( net, tr, inputs,outputs)

    predOutputs=net(inputs');

    %Train statistics
    trainTargets = gmultiply(outputs',tr.trainMask{1,1});
    trainP = perform(net,trainTargets,predOutputs);
    aux=corrcoef(trainTargets(tr.trainInd)',predOutputs(tr.trainInd)');
    trainR=aux(2,1);
    %Validation statistics
    valTargets = gmultiply(outputs',tr.valMask{1,1});
    valP = perform(net,valTargets,predOutputs);
    aux=corrcoef(valTargets(tr.valInd)',predOutputs(tr.valInd)');
    valR=aux(2,1);

    %Testing statistics
    testTargets = gmultiply(outputs',tr.testMask{1,1});
    testpred=predOutputs(tr.testInd)';
    testP = perform(net,testTargets,predOutputs);
    aux=corrcoef(testTargets(tr.testInd)',predOutputs(tr.testInd)');
    testR=aux(2,1);

end

