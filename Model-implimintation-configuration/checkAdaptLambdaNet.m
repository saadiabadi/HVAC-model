function [Tra, Tpa, predOutputs ] = checkAdaptLambdaNet( net, inputs,outputs, trainIdx,valIdx,testIdx,seed,varargin )

pattern_block=1008;
%pattern_block=6000;
trainRatio=0.75;
valRatio=0.25;

predOutputs=[];
pattern_range=1:pattern_block:length(testIdx);
tperf=[];j=1;

for i=1:length(pattern_range)
     
     if i==length(pattern_range)
         testRange=pattern_range(i):length(testIdx);
     else
         testRange=pattern_range(i):pattern_range(i+1)-1;%index block used to test the net rigth now. It symbolize the patterns in one day
     end

       testBlock=testIdx(testRange);%We obtain the specific test index related to the index block 
       testInputData=inputs(testBlock,:);% We obtain the patterns linked to the selected index
       testOutputData=outputs(testBlock,:);% We obtain the patterns linked to the selected index
       
       for j=1:size(testInputData,1)
            predOutputs=[predOutputs net(testInputData(j,:)')];
            tper(j) = perform(net,testOutputData(j,:), net(testInputData(j,:)'));
            net=adapt(net,testInputData(j,:)',testOutputData(j,:)');
            j=j+1;
       end
       
       

        if i~=length(pattern_range)
            [trainIdx valIdx]=distributeData(trainIdx,valIdx,testBlock,trainRatio, valRatio);%We distribute the test data between the trainSet and the Validation set in order to train the net again with more data
            [ bestNet bestTr bestTrainP bestTrainR bestValP bestValR bestTestP bestTestR ]=checkFitNet(inputs,outputs, trainIdx,valIdx,testIdx,seed);
             net=bestNet;
            
        end    
end

 %Testing statistics
  
    aux=corrcoef(outputs(testIdx)',predOutputs);
%     testR=aux(2,1);
    Tra=aux(2,1);
    Tpa=mean(tper);
%     disp('Adapt-Lambda TestR: ')
%     disp(testR);




end
