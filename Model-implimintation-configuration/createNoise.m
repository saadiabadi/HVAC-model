function [ inputs ] = createNoise( inputs, cols,idx,perc,seed )
        s = RandStream('twister','Seed',seed);
        RandStream.setGlobalStream(s);
        
         for col=cols
            elements=size(idx,2);
            randidx=randperm(elements,ceil(elements*perc));
  
            noise=awgn(inputs(idx(1,randidx),col),10,'measured');
            inputs(idx(1,randidx),col)=noise;
         end
        


end

