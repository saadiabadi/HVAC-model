

function [Tr,Tp,predOutputs ] = checkAdaptNet( net, inputs,outputs, trainIdx,valIdx,testIdx,seed,varargin )



predOutputs=[];
tperf=[];j=1;

for pattern=testIdx
      predOutputs=[predOutputs net(inputs(pattern,:)')];
      tper(j) = perform(net,outputs(pattern,:), net(inputs(pattern,:)'));
      net=adapt(net,inputs(pattern,:)',outputs(pattern,:)');
      j=j+1;
end



 %Testing statistics
  
    aux=corrcoef(outputs(testIdx)',predOutputs);
    Tr=aux(2,1);
    Tp=mean(tper);
%     disp('Adapt Net TestR: ')
%     disp(TR);

end
