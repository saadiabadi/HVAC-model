function [ bestNet bestTr bestTrainP bestTrainR bestValP bestValR bestTestP bestTestR ] = checkFitNet(  inputs,outputs, trainIdx,valIdx,testIdx,seed,varargin)

display_sms=false;
max_neur=2*size(inputs,2);
neur_range=[1:max_neur];
if nargin<9 
    epochs=1000; 
end
if nargin<8 
    lr=0.01;
end

max_fail=epochs;
window=false;
best_train_performance=zeros(1,max_neur);
best_train_R=zeros(1,max_neur);
best_val_performance=zeros(1,max_neur);
best_val_R=zeros(1,max_neur);
best_test_performance=zeros(1,max_neur);
best_test_R=zeros(1,max_neur);
avgPerformance=zeros(1,size(trainIdx,1));
avgRfactor=zeros(1,size(trainIdx,1));
best_nets={};
best_tr={};


  parfor hiddenLayerSize=neur_range 

        s = RandStream('twister','Seed',seed);
        RandStream.setGlobalStream(s);



        [net,tr]=obtainFitNet(hiddenLayerSize,epochs,lr,window,trainIdx,valIdx,testIdx, inputs,outputs);
       

        [trainP trainR valP valR testP testR]=getPerformanceNet(net,tr,inputs,outputs);




        %We are printing the gradient evolution in order to know if the LR
        %is small enought
%             fig=figure;
%             set(fig, 'Visible', 'off');
%             plot(tr.gradient);
%             name=sprintf('FitNetGradient_Neur_%i_LR_%f',hiddenLayerSize,lr);
%             print(fig,strcat(path,name),'-dpng');
%             close(fig);




        if display_sms
            sms=sprintf('Regular validation;Epochs: %i; Neurons: %i; Learning rate: %f;Train R: %f; Train Performance:%f; Val R coefficient: %f;  Val Performance:%f; Test R coefficient: %f;  Test Performance:%f ',epochs, hiddenLayerSize,lr,trainR,trainP,valR,valP,testR,testP);
            disp(sms);
        end
        
        best_train_performance(hiddenLayerSize)=trainP;
        best_train_R(hiddenLayerSize)=trainR;

        best_val_performance(hiddenLayerSize)=valP;
        best_val_R(hiddenLayerSize)=valR;

        best_test_performance(hiddenLayerSize)=testP;
        best_test_R(hiddenLayerSize)=testR;

        best_nets{hiddenLayerSize}=net;
        
        best_tr{hiddenLayerSize}=tr;



  end

   [value ind]=min(best_val_performance);
    if display_sms
        sms=sprintf('Best ANN;  Neurons: %i;  Val R coefficient: %f;  Val Performance:%f; Test R coefficient: %f;  Test Performance:%f ',ind(end),best_val_R(ind(end)),best_val_performance(ind(end)),best_test_R(ind(end)),best_test_performance(ind(end)) );
        disp(sms);
    end
    sms=sprintf('Neurons: %i; ',ind(end));
    disp(sms);
    
    
    bestNet=best_nets{ind(end)};
    bestTr=best_tr{ind(end)};
    bestTrainP=best_train_performance(ind(end));
    bestTrainR=best_train_R(ind(end));
    bestValP=best_val_performance(ind(end));
    bestValR=best_val_R(ind(end));
    bestTestP=best_test_performance(ind(end));
    bestTestR=best_test_R(ind(end));

    
        
    

