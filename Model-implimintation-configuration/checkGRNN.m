function [ output_args ] = checkGRNN(inputs,outputs, trainIdx,valIdx,testIdx,varargin)

if nargin<6
    spread_range=[0.001,0.005,0.01,0.05,0.1,0.3,0.5,0.7,0.9,1,1.3,1.5,1.7,2];
end



for idx=1:size(trainIdx,1)
    best_val_performance=zeros(1,length(spread_range));
    best_val_R=zeros(1,length(spread_range));
    best_test_performance=zeros(1,length(spread_range));
    best_test_R=zeros(1,length(spread_range));
    avgPerformance=zeros(1,size(trainIdx,1));
    avgRfactor=zeros(1,size(trainIdx,1));
     for spread=1:length(spread_range)
         s = RandStream('twister','Seed',spread);
         RandStream.setGlobalStream(s); 
         %%%
         %%Start network configuration
         %%%
         trainingPatterns=inputs(trainIdx(idx,:));
         trainingOutputs=outputs(trainIdx(idx,:));
         net=newgrnn(trainingPatterns,trainingOutputs',spread_range(spread));
         %net.inputs{1}.processFcns = {'removeconstantrows','mapminmax'};
       %  net.outputs{2}.processFcns = {'removeconstantrows','mapminmax'};
         %%%
         %%End network configuration
         %%%
         
         predValOutputs=sim(net,inputs(valIdx(idx,:)));
         valOutputs=outputs(valIdx(idx,:))';
         
         valMSE=mean((valOutputs-predValOutputs).^2);
         aux=corrcoef(predValOutputs,valOutputs);
         valR=aux(2,1);
         
         best_val_performance(spread)=valMSE;
         best_val_R(spread)=valR;
     end
     
       [value ind]=min(best_val_performance);
end
end

