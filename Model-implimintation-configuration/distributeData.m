function [ trainIdx, valIdx] = distributeData(trainCurrentIdx,valCurrentIdx, newIdx, trainRatio,valRatio)

[trainInd,valInd,testInd] = dividerand(length(newIdx),trainRatio,valRatio,0);

trainIdx=[trainCurrentIdx newIdx(trainInd)];
valIdx=[valCurrentIdx newIdx(valInd)];

end

